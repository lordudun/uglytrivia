Legacy Code Retreat Ugly Trivia
--------------------------------

Legacy Code Retreat for Ugly Trivia. Session sobre codigo legado del PMA con Jordi Salvat (@jsalvata)

Evoluciones
-----------

- Juego para 6 jugadores como maximo
- Juego para un numero infinito de jugadores
- Internacionalizacion de los mensajes
- Correcion de los textos
- Numero variable de categorias
