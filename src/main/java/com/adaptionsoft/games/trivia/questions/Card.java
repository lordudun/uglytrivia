package com.adaptionsoft.games.trivia.questions;

public class Card {
	
	private String category;
	private String identificator;
	
	public Card (String category, int cardNumber) {
		this.category = category;
		this.identificator = Integer.toString(cardNumber);
	}
	
	public String getCategory() {
		
		return category;
	}
	
	public String getIdentificator() {
		
		return identificator;
	}
}
