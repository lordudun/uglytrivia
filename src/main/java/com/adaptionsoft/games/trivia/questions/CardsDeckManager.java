package com.adaptionsoft.games.trivia.questions;

import java.util.Hashtable;
import java.util.Map;

public class CardsDeckManager {
	
	private Map<Integer,CardsDeck> decks;
	
	public CardsDeckManager(String[] decksCategoriesNames) {
		
		this.decks = new Hashtable<Integer,CardsDeck>();
		
		prepareDecks(decksCategoriesNames);
	}
	
	private void prepareDecks(String[] decksCategoriesNames) {
		
		CardsDeck newDeck;
		
		for (int currentDeck = 0; currentDeck < decksCategoriesNames.length; currentDeck++) {
			
			newDeck = new CardsDeck(decksCategoriesNames[currentDeck]);
			decks.put(currentDeck, newDeck);
		}
	}
	
	public void addCardsToTheDecks(int cardsNumber) {
		
		for(CardsDeck currentDeck: decks.values()){
			
			currentDeck.addCards(cardsNumber);
		}
	}
	
	public Card showCardByLocation(int location) {
		
		CardsDeck locationDeck = categoryOfTheLocation(location);
		return locationDeck.nextCard();	
	}
	
	private CardsDeck categoryOfTheLocation(int location) {
	
		int locationCategory = location % decks.size();
		return decks.get(locationCategory);
	}
}
