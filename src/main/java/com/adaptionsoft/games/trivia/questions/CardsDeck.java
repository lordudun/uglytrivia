package com.adaptionsoft.games.trivia.questions;

import java.util.LinkedList;

public class CardsDeck {
	
	protected LinkedList<Card> cards;
	private String categoryName;
	
	public CardsDeck(String categoryName) {
	
		cards = new LinkedList<Card>();
		this.categoryName = categoryName.toUpperCase();
	}
	
	public void addCards(int cardsNumber) {
		
		Card newCard;
		int lastCard = cards.size() + cardsNumber;
		
		for (int currentCard = cards.size(); currentCard < lastCard; currentCard++) {
			
			newCard = new Card(categoryName, currentCard);
			cards.addLast(newCard);
		}
	}
	
	public String categoryName() {
		
		return categoryName;
	}
	
	public Card nextCard() {
		
		return cards.removeFirst();
	}
}
