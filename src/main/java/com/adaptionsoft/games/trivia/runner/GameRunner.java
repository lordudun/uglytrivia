package com.adaptionsoft.games.trivia.runner;
import java.util.Random;

import com.adaptionsoft.games.trivia.game.Game;
import com.adaptionsoft.games.trivia.players.Player;


public class GameRunner {
	
	private static String[] categoriesNames = {"POP","SCIENCE","SPORTS","ROCK"};
	private static boolean winner;
	
	public static void main(String[] args) {
		
		Game game = null;;
		Random rand = new Random();
		
		initializeGame(game);
		play(game, rand);
	}

	static void play(Game aGame, Random rand) {
		do {
			
			aGame.roll(rand.nextInt(5) + 1);
			winner = aGame.answerQuestion(rand.nextInt(9));	
			
		} while (!winner);
	}

	static void initializeGame(Game game) {
		
		Player newPlayer;
		game = new Game(categoriesNames);
		
		newPlayer = new Player("Chet");
		game.add(newPlayer);
		newPlayer = new Player("Pat");
		game.add(newPlayer);
		newPlayer = new Player("Sue");
		game.add(newPlayer);
	}
}
