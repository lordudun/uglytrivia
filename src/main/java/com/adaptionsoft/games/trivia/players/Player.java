package com.adaptionsoft.games.trivia.players;


public class Player {
	
	private String name;
	private Integer place;
	private Integer coins;
	private Boolean inPenaltyBox;
	
	private static final int MaxPlace = 12;
	
	public Player (String name) {
		
		this.name = name;
	    place = 0;
	    coins = 0;
	    inPenaltyBox = false;    
	}
	
	public int getCoins() {
		return coins;
	}
	
	public String getName() {
		return name;
	}
	
	public boolean isInPenaltyBox() {
		return inPenaltyBox;
	}
	
	public void putInPenaltyBox() {
		inPenaltyBox = true;
	}
	
	public void addGoldCoint() {
		coins++;
	}
	
	public int roll(int positions){
		
		place = (place + positions) % MaxPlace;
		return place;
	}
}
