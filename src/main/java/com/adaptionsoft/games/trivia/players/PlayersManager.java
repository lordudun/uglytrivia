package com.adaptionsoft.games.trivia.players;

import java.util.HashMap;
import java.util.Map;

public class PlayersManager {
	
	private Map<Integer,Player> playersData;
	private int currentPlayer = 0;
	
	public PlayersManager() {
		
		playersData = new HashMap<Integer,Player>();
	}
	
    public int addPlayer(Player player) {
		
    	int playerId = howManyPlayers();
    	playersData.put(playerId, player);
	    
	    return howManyPlayers();
    }
    
    public Player getCurrentPlayer() {
    	return playersData.get(currentPlayer);
    }
	
	private int howManyPlayers() {
			return playersData.size();
	}
	
	public void nextPlayer() {
		currentPlayer = (currentPlayer+1) % howManyPlayers();
	}
}