package com.adaptionsoft.games.trivia.game;

import com.adaptionsoft.games.trivia.players.Player;
import com.adaptionsoft.games.trivia.players.PlayersManager;
import com.adaptionsoft.games.trivia.questions.Card;
import com.adaptionsoft.games.trivia.questions.CardsDeckManager;

public class Game {
	
	private static final String questions_path = "_QUESTION";
	private static final int bad_response = 7; 
	private static final int coins_to_win = 6;
	private static final int card_by_player = 20;
	
	private PlayersManager gamePlayers;
    private CardsDeckManager cards;
    private Messages messages;
	boolean isGettingOutOfPenaltyBox;

    public Game(String[] categoriesNames) {
    	
    	gamePlayers = new PlayersManager();
    	messages = new Messages();
    	cards = new CardsDeckManager(categoriesNames);
    }
    
    public boolean add(Player player) {
	    
    	int playerNumber = gamePlayers.addPlayer(player);
	    cards.addCardsToTheDecks(card_by_player);
	    
	    System.out.println(player.getName() + messages.getMessage("WAS_ADDED"));
	    System.out.println(messages.getMessage("PLAYER_NUMBER") + playerNumber);
	    
		return true;
	}

	public void roll(int roll) {
		
		Player currentPlayer = gamePlayers.getCurrentPlayer();
		System.out.println(currentPlayer.getName() + messages.getMessage("CURRENT_PLAYER"));
		System.out.println(messages.getMessage("ROLLED") + roll);
		
		if (isNotGettingOutOfPenaltyBox(roll)) {
			
			System.out.println(currentPlayer.getName() + messages.getMessage("NOT_GETTING_OUT_PENALTY"));
			isGettingOutOfPenaltyBox = false;
			
		} else {
			
			if (currentPlayer.isInPenaltyBox()) {
				
				isGettingOutOfPenaltyBox = true;
				System.out.println(currentPlayer.getName() + messages.getMessage("GETTING_OUT_PENALTY"));
			}
			
			int newLocation = rollToNewLocation(roll);
			Card card = cards.showCardByLocation(newLocation);
			
			System.out.println(messages.getMessage("CATEGORY") + messages.getMessage(card.getCategory()));
			System.out.println(messages.getMessage(card.getCategory() + questions_path ) + card.getIdentificator());	
		}
	}

	private int rollToNewLocation(int positions) {
		
		Player currentPlayer = gamePlayers.getCurrentPlayer();
		int newLocation = currentPlayer.roll(positions);
		
		System.out.println(currentPlayer.getName() 
				+ messages.getMessage("LOCATION_IS")
				+ newLocation);
		
		return newLocation;
	}
	
	public boolean answerQuestion(int response) {
		
		if (response == bad_response) {
			
			wrongAnswer();
		} else {
			
			wasCorrectlyAnswered();
		}
		
		return endPlayerTurn();
	}
	
	private void wrongAnswer(){
		
		Player currentPlayer = gamePlayers.getCurrentPlayer();
		
		System.out.println(messages.getMessage("INCORRECTLY_ANSWERED"));
		System.out.println(currentPlayer.getName() + messages.getMessage("TO_PENALTY_BOX"));			
		currentPlayer.putInPenaltyBox();
	}

	private void wasCorrectlyAnswered() {
		
		if (!inPenaltyBoxAndNotGettingOut()) {

			System.out.println(messages.getMessage("ANSWER_CORRECT"));
			correctlyAnswered();
		}
	}
	
	private void correctlyAnswered() {
		
		Player currentPlayer = gamePlayers.getCurrentPlayer();
		currentPlayer.addGoldCoint();
		
		System.out.println(currentPlayer.getName()
				+ messages.getMessage("NOW_HAS")
				+ currentPlayer.getCoins()
				+ messages.getMessage("GOLD_COINS"));
	}
	
	private boolean endPlayerTurn() {
		boolean winner = didPlayerWin();
		gamePlayers.nextPlayer();
		return winner;
	}
	
	private boolean inPenaltyBoxAndNotGettingOut() {
		return (gamePlayers.getCurrentPlayer().isInPenaltyBox() && !isGettingOutOfPenaltyBox);
	}
	
	private boolean isNotGettingOutOfPenaltyBox(int roll) {
		
		return gamePlayers.getCurrentPlayer().isInPenaltyBox() && (roll % 2 == 0);
	}
	
	private boolean didPlayerWin() {
		
		boolean winner;
		
		if (inPenaltyBoxAndNotGettingOut()) {
			
			winner = false;
		} else {
			
			winner = (gamePlayers.getCurrentPlayer().getCoins() == coins_to_win);
		}
		
		return winner; 
	}
}
