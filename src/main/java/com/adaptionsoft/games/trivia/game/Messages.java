package com.adaptionsoft.games.trivia.game;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class Messages {
	private static final String BUNDLE_NAME = "com.adaptionsoft.games.uglytrivia.messages";

	private ResourceBundle RESOURCE_BUNDLE;
	
	public Messages() {
		
		this.RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);
	}
	
	public Messages(ResourceBundle RESOURCE_BUNDLE) {
		
		this.RESOURCE_BUNDLE = RESOURCE_BUNDLE;
	}

	public String getMessage(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}
