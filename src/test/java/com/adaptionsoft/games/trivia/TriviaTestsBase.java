
package com.adaptionsoft.games.trivia;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Random;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;
import java.util.zip.Checksum;

import com.adaptionsoft.games.trivia.game.Game;
import com.adaptionsoft.games.trivia.players.Player;

public abstract class TriviaTestsBase {

	protected static final String[] default_categories = {"POP","SCIENCE","SPORTS","ROCK"};
	protected static final String resourcesBasePath = "src/test/resources/com/adaptionsoft/games/trivia/";
	
	protected Checker checker;
	protected Random rand;
	protected Game game;
	
	public class Checker extends OutputStream {
		public Checksum checksum = new CRC32();
		@Override
		public void write(int b) throws IOException {
			checksum.update(b);
		}
	}
	
	protected void initialize() {
		checker = new Checker();
		System.setOut(new PrintStream(checker));
		rand = new Random(0L);
	}
	
	protected void addPlayerToGame(String playerName) {
		
		Player newPlayer = new Player(playerName);
		game.add(newPlayer);
	}
	
	protected void addPlayers(int playersNumber) {
		
		for (int currentPlayer=0; currentPlayer < playersNumber; currentPlayer++) {
			
			addPlayerToGame("Player " + currentPlayer);
		}
	}

	protected long prepareExpectedOut(String filePath) throws IOException {
		
		CheckedInputStream checkstream = null;
        long checksum = 0L;
        
        try {
        	
	        try {
	        	checkstream = new CheckedInputStream(
	                    new FileInputStream(filePath), new CRC32());
	        	
	        } catch (FileNotFoundException e) {
	            System.err.println("File not found.");
	            System.exit(1);
	        }
	
	        byte[] buf = new byte[128];
	        while(checkstream.read(buf) >= 0) {
	        }
	        
	        checksum = checkstream.getChecksum().getValue();
	        
        } catch (IOException e) {
        	e.printStackTrace();
        	System.exit(1);
        } finally {
        	checkstream.close();
        }
        
        return checksum;
	}
}
