package com.adaptionsoft.games.trivia.questions;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TakeCardsByLocation {
	
	private CardsDeckManager deckManager;
	private Card card;
	
	@Test
	public void one_category() {
		
		String[] categories = {"Pop"};
		prepareCardsDeck(categories);
		
		card = deckManager.showCardByLocation(0);
		
		assertCard("POP", "0");
	}

	@Test
	public void three_categories() {
		
		String[] categories = {"Pop","Rock","Maths"};
		prepareCardsDeck(categories);
		
		card = deckManager.showCardByLocation(1);
		assertCard("ROCK", "0");
		
		card = deckManager.showCardByLocation(2);
		assertCard("MATHS", "0");
		
		card = deckManager.showCardByLocation(3);
		assertCard("POP", "0");
		
		card = deckManager.showCardByLocation(4);
		assertCard("ROCK", "1");
	}
	
	private void prepareCardsDeck(String[] categories) {
		
		deckManager = new CardsDeckManager(categories);
		deckManager.addCardsToTheDecks(2);
	}
	
	private void assertCard(String expectedCategory, String expectedIdentificator) {
		assertEquals(expectedCategory, card.getCategory());
		assertEquals(expectedIdentificator, card.getIdentificator());
	}
}