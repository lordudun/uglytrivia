package com.adaptionsoft.games.trivia.questions;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TakeCardsFromDeck {
	
	private static final String undefined_category = "UNDEFINED";
	
	private CardsDeck deck;
	private Card card;
	
	@Before
	public void setUp() {
		
		deck = new CardsDeck(undefined_category);
	}
	
	@Test
	public void one_card() {
		
		deck.addCards(1);
		card = deck.nextCard();
		
		assertCard(undefined_category, "0");
	}
	
	@Test
	public void three_cards() {
		
		deck.addCards(3);
		
		card = deck.nextCard();
		assertCard(undefined_category, "0");
		
		card = deck.nextCard();
		assertCard(undefined_category, "1");
		
		card = deck.nextCard();
		assertCard(undefined_category, "2");
	}

	private void assertCard(String expectedCategory, String expectedIdentificator) {
		assertEquals(expectedCategory, card.getCategory());
		assertEquals(expectedIdentificator, card.getIdentificator());
	}
}
