package com.adaptionsoft.games.trivia.players;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Test;

public class AddNewPlayer {

	private PlayersManager gamePlayers;
	
	private Player mockedPlayer = mock(Player.class);
	
	@Before
	public void setUp() {
		
		gamePlayers = new PlayersManager();
	}
	
	@Test
	public void add_one_player_then_the_id_is_one() {
		
		assertEquals(1, gamePlayers.addPlayer(mockedPlayer));
	}
	
	@Test
	public void add_two_players_then_the_id_is_two() {
		
		gamePlayers.addPlayer(mockedPlayer);
		
		assertEquals(2, gamePlayers.addPlayer(mockedPlayer));
	}
}
