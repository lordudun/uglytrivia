package com.adaptionsoft.games.trivia.players;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class PlayerActions {

	private Player player;
	
	@Before
	public void setUp() {
		
		player = new Player("Lisa");
	}
	
	@Test
	public void players_name_is_Lisa() {
		
		assertEquals("Lisa", player.getName());
	}
	
	@Test
	public void new_player_coins_are_0() {
		
		assertEquals(0, player.getCoins());
	}
	
	@Test
	public void player_coins_are_1_after_add_gold_coin() {
		
		player.addGoldCoint();
		
		assertEquals(1, player.getCoins());
	}
	
	@Test
	public void new_player_is_not_in_penaly_box() {
		
		assertFalse(player.isInPenaltyBox());
	}
	
	@Test
	public void player_is_in_penalty_box_after_put_int() {
		
		player.putInPenaltyBox();
		
		assertTrue(player.isInPenaltyBox());
	}
	
	@Test
	public void player_rolls_zero_positions_are_in_the_same_site() {
		
		assertEquals(0, player.roll(0));
	}
	
	@Test
	public void player_rolls_one_positions_are_in_site_one() {
		
		assertEquals(1, player.roll(1));
	}
	
	@Test
	public void player_rolls_eleven_positions_are_in_site_eleven() {
		
		player.roll(10);
		
		assertEquals(11, player.roll(1));
	}
	
	@Test
	public void player_rolls_twelve_positions_are_in_site_zero() {
		
		player.roll(8);
		
		assertEquals(0, player.roll(4));
	}
}
