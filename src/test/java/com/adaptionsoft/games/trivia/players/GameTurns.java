package com.adaptionsoft.games.trivia.players;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

public class GameTurns {

	private PlayersManager gamePlayers;
	
	private Player mockedPlayer_1 = mock(Player.class);
	private Player mockedPlayer_2 = mock(Player.class);
	
	@Before
	public void setUp() {
		
		gamePlayers = new PlayersManager();
		gamePlayers.addPlayer(mockedPlayer_1);
	}
	
	@Test
	public void firts_turn_player() {
		
		when(mockedPlayer_1.getName()).thenReturn("Player 1");
		
		assertEquals("Player 1", gamePlayers.getCurrentPlayer().getName());
	}
	
	@Test
	public void second_turn_only_one_player() {
		
		when(mockedPlayer_1.getName()).thenReturn("Player 1");
		gamePlayers.nextPlayer();
		
		assertEquals("Player 1", gamePlayers.getCurrentPlayer().getName());
	}
	
	@Test
	public void second_turn_only_two_players() {
		
		when(mockedPlayer_2.getName()).thenReturn("Player 2");
		gamePlayers.addPlayer(mockedPlayer_2);
		gamePlayers.nextPlayer();
		
		assertEquals("Player 2", gamePlayers.getCurrentPlayer().getName());
	}
}
