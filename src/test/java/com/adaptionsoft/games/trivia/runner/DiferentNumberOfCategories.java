package com.adaptionsoft.games.trivia.runner;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import com.adaptionsoft.games.trivia.TriviaTestsBase;
import com.adaptionsoft.games.trivia.game.Game;

public class DiferentNumberOfCategories extends TriviaTestsBase {
	
	private static final String resourcesSpecificPath = resourcesBasePath + "runner/questions/";
	
	@Before
	public void setUp() {
		
		initialize();
	}

	@Test
	public void one_category() throws IOException {
		
		long expectedResult = prepareExpectedOut(resourcesSpecificPath + "oneCategory.out");
		
		String[] categories = {"Rock"};
		
		game = new Game(categories);
		addPlayers(3);
		GameRunner.play(game, rand);
		
		assertEquals(expectedResult , checker.checksum.getValue());
		checker.close();
	}
	
	@Test
	public void six_categories() throws IOException {
		
		long expectedResult = prepareExpectedOut(resourcesSpecificPath + "sixCategories.out");
		
		String[] categories = {"Rock","Pop","Science","Sports","Maths","Cinema"};
		
		game = new Game(categories);
		addPlayers(3);
		GameRunner.play(game, rand);
		
		assertEquals(expectedResult , checker.checksum.getValue());
		checker.close();
	}
}
