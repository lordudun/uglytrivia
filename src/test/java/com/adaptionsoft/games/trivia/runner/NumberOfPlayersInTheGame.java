package com.adaptionsoft.games.trivia.runner;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import com.adaptionsoft.games.trivia.TriviaTestsBase;
import com.adaptionsoft.games.trivia.game.Game;

public class NumberOfPlayersInTheGame extends TriviaTestsBase {
	
	private static final String resourcesSpecificPath = resourcesBasePath + "runner/players/";
	private int infinite_number_of_players = 100;
	
	@Before
	public void setUp() {
		
		initialize();
		game = new Game(default_categories);
	}

	@Test
	public void three_players() throws IOException {
		
		long expectedResult = prepareExpectedOut(resourcesSpecificPath + "three.out");
		
		addPlayers(3);
		GameRunner.play(game, rand);
		
		assertEquals(expectedResult, checker.checksum.getValue());
		checker.close();
	}
	
	@Test
	public void six_players() throws IOException {
		
		long expectedResult = prepareExpectedOut(resourcesSpecificPath + "six.out");
		
		addPlayers(6);
		GameRunner.play(game, rand);
		
		assertEquals(expectedResult, checker.checksum.getValue());
		checker.close();
	}
	
	@Test
	public void ten_players() throws IOException {
		
		long expectedResult = prepareExpectedOut(resourcesSpecificPath + "ten.out");
		
		addPlayers(10);
		GameRunner.play(game, rand);
		
		assertEquals(expectedResult, checker.checksum.getValue());
		checker.close();
	}
	
	@Test
	public void infinite_number_of_players() throws IOException {
		
		long expectedResult = prepareExpectedOut(resourcesSpecificPath + "infinite.out");
		
		addPlayers(infinite_number_of_players);
		GameRunner.play(game, rand);
		
		assertEquals(expectedResult, checker.checksum.getValue());
		checker.close();
	}
}
