package com.adaptionsoft.games.trivia.runner;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;

import com.adaptionsoft.games.trivia.TriviaTestsBase;
import com.adaptionsoft.games.trivia.game.Game;

public class DiferentLanguages extends TriviaTestsBase {
	
	private static final String resourcesSpecificPath = resourcesBasePath + "runner/languages/";
	
	@Before
	public void setUp() {
		initialize();
	}
	
	@Test
	public void for_three_default_language_players() throws IOException {
		
		long expectedResult = prepareExpectedOut(resourcesSpecificPath + "default.out");
		
		game = new Game(default_categories);
		addPlayerToGame("A");
		addPlayerToGame("B");
		addPlayerToGame("C");
		
		GameRunner.play(game, rand);
		
		assertEquals(expectedResult, checker.checksum.getValue());
		checker.close();
	}
	
	@Test
	public void for_three_spanish_players() throws IOException {
		
		long expectedResult = prepareExpectedOut(resourcesSpecificPath + "spanish.out");
	    Locale.setDefault(new Locale("ES"));
	    
	    game = new Game(default_categories);
		addPlayerToGame("Jose");
		addPlayerToGame("Maria");
		addPlayerToGame("Pedro");
		
		GameRunner.play(game, rand);
		
		assertEquals(expectedResult, checker.checksum.getValue());
		checker.close();
	}
	
	@Test
	public void for_three_english_players() throws IOException {
		
		long expectedResult = prepareExpectedOut(resourcesSpecificPath + "english.out");
		Locale.setDefault(new Locale("en"));
		
		game = new Game(default_categories);
		addPlayerToGame("John");
		addPlayerToGame("Lisa");
		addPlayerToGame("Peter");
		
		GameRunner.play(game, rand);
		
		assertEquals(expectedResult, checker.checksum.getValue());
		checker.close();
	}
}
