package com.adaptionsoft.games.trivia.game;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

import org.junit.Before;
import org.junit.Test;

import com.adaptionsoft.games.trivia.game.Messages;

public class ShowMessages {

	private MockedResourceBundle Mocked_RESOURCE_BUNDLE;
	private Messages messages;
	private String UNDEFINED_STRING = "Undefined String";
	
	class MockedResourceBundle extends ResourceBundle {

		@Override
		public Enumeration<String> getKeys() {
			
			return null;
		}

		@Override
		protected Object handleGetObject(String key) {
			
			return UNDEFINED_STRING;
		}
	}
	
	@Before
	public void setUp() {
		
		Mocked_RESOURCE_BUNDLE = new MockedResourceBundle();
		messages = new Messages(Mocked_RESOURCE_BUNDLE);
	}

	@Test
	public void default_languaje() throws IOException {
		
		assertEquals(UNDEFINED_STRING, messages.getMessage(UNDEFINED_STRING));
	}
	
	@Test
	public void spanish_languaje() throws IOException {
		
		Locale.setDefault(new Locale("es"));
		assertEquals(UNDEFINED_STRING, messages.getMessage(UNDEFINED_STRING));
	}
}
